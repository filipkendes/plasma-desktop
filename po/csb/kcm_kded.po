# translation of kcmkded.po to Kashubian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Michôł Òstrowsczi <michol@linuxcsb.org>, 2007.
# Mark Kwidzińsczi <mark@linuxcsb.org>, 2008.
# Mark Kwidzińśczi <mark@linuxcsb.org>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcmkded\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-28 00:39+0000\n"
"PO-Revision-Date: 2009-10-20 18:13+0200\n"
"Last-Translator: Mark Kwidzińśczi <mark@linuxcsb.org>\n"
"Language-Team: Kaszëbsczi <i18n-csb@linuxcsb.org>\n"
"Language: csb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2)\n"

#: kcmkded.cpp:115
#, fuzzy, kde-format
#| msgid "Unable to stop server <em>%1</em>."
msgid "Failed to stop service: %1"
msgstr "Nie mòże zatrzëmac serwerë <em>%1</em>."

#: kcmkded.cpp:117
#, fuzzy, kde-format
#| msgid "Unable to start server <em>%1</em>."
msgid "Failed to start service: %1"
msgstr "Nie mòże wësztartowac serwerë <em>%1</em>."

#: kcmkded.cpp:124
#, fuzzy, kde-format
#| msgid "Unable to stop server <em>%1</em>."
msgid "Failed to stop service."
msgstr "Nie mòże zatrzëmac serwerë <em>%1</em>."

#: kcmkded.cpp:126
#, fuzzy, kde-format
#| msgid "Unable to start server <em>%1</em>."
msgid "Failed to start service."
msgstr "Nie mòże wësztartowac serwerë <em>%1</em>."

#: kcmkded.cpp:224
#, kde-format
msgid "Failed to notify KDE Service Manager (kded6) of saved changed: %1"
msgstr ""

#: ui/main.qml:40
#, kde-format
msgid ""
"The background services manager (kded6) is currently not running. Make sure "
"it is installed correctly."
msgstr ""

#: ui/main.qml:50
#, kde-format
msgid ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."
msgstr ""

#: ui/main.qml:60
#, kde-format
msgid ""
"Some services were automatically started/stopped when the background "
"services manager (kded6) was restarted to apply your changes."
msgstr ""

#: ui/main.qml:108
#, fuzzy, kde-format
#| msgid "Service"
msgid "All Services"
msgstr "Ùsłëżnota"

#: ui/main.qml:109
#, fuzzy, kde-format
#| msgid "Running"
msgctxt "List running services"
msgid "Running"
msgstr "Zrëszony"

#: ui/main.qml:110
#, fuzzy, kde-format
#| msgid "Not running"
msgctxt "List not running services"
msgid "Not Running"
msgstr "Niézrëszony"

#: ui/main.qml:147
#, fuzzy, kde-format
#| msgid "Startup Services"
msgid "Startup Services"
msgstr "Sztartowóné ùsłëżnotë"

#: ui/main.qml:148
#, kde-format
msgid "Load-on-Demand Services"
msgstr "Ùsłëżnotë Wladëjë-Na-Żądanié"

#: ui/main.qml:167
#, kde-format
msgctxt "@action:button %1 service name"
msgid "Disable automatically loading %1 on startup"
msgstr ""

#: ui/main.qml:167
#, kde-format
msgctxt "@action:button %1 service name"
msgid "Enable automatically loading %1 on startup"
msgstr ""

#: ui/main.qml:168
#, kde-format
msgid "Toggle automatically loading this service on startup"
msgstr ""

#: ui/main.qml:212
#, kde-format
msgid "Not running"
msgstr "Niézrëszony"

#: ui/main.qml:213
#, kde-format
msgid "Running"
msgstr "Zrëszony"

#: ui/main.qml:233
#, fuzzy, kde-format
#| msgid "Stop"
msgctxt "@action:button %1 service name"
msgid "Stop %1"
msgstr "Zatrzëmôj"

#: ui/main.qml:233
#, fuzzy, kde-format
#| msgid "Start"
msgctxt "@action:button %1 service name"
msgid "Start %1"
msgstr "Sztart"

#: ui/main.qml:236
#, fuzzy, kde-format
#| msgid "Service"
msgid "Stop Service"
msgstr "Ùsłëżnota"

#: ui/main.qml:236
#, fuzzy, kde-format
#| msgid "Service"
msgid "Start Service"
msgstr "Ùsłëżnota"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Michôł Òstrowsczi, Mark Kwidzińsczi"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "michol@linuxcsb.org, mark@linuxcsb.org"

#, fuzzy
#~| msgid "Startup Services"
#~ msgid "Background Services"
#~ msgstr "Sztartowóné ùsłëżnotë"

#, fuzzy
#~| msgid "(c) 2002 Daniel Molkentin"
#~ msgid "(c) 2002 Daniel Molkentin, (c) 2020 Kai Uwe Broulik"
#~ msgstr "(c) 2002 Daniel Molkentin"

#~ msgid "Daniel Molkentin"
#~ msgstr "Daniel Molkentin"

#~ msgid "kcmkded"
#~ msgstr "kcmkded"

#~ msgid "KDE Service Manager"
#~ msgstr "Menadżera ùsłëżnotów KDE"

#~ msgid "Status"
#~ msgstr "Sztatus"

#~ msgid "Description"
#~ msgstr "Òpisënk"

#~ msgid "Use"
#~ msgstr "Brëkùnk"

#~ msgid "Unable to contact KDED."
#~ msgstr "Ni mòże sparłãczëc sã z KDED"

#~ msgid "Unable to start service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr ""
#~ "Nie mòże wësztartowac ùsłëżnotë <em>%1</em>, <br /><br /><i>Fela: %2</i>"

#~ msgid "Unable to stop service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr ""
#~ "Nié mòże zatrzëmac ùsłëżnotë<em> %1</em> serwerë.<br /><br /><i>Fela: %2</"
#~ "i>"
